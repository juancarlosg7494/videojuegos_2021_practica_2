using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedController : MonoBehaviour
{
    
    private Rigidbody2D rb;
    private Animator animator;
    public float velocidad = 6;
    public float salto = 40;
    public int pasados = 0;
    private const string tag_enemigo = "Enemigo";
    
    private const string tag_cabeza = "Cabeza";
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        animator.SetInteger("Estado", 0);
        rb.velocity = new Vector2(velocidad, rb.velocity.y);
        if (Input.GetKeyUp(KeyCode.Space))
        {
            rb.AddForce(Vector2.up * salto, ForceMode2D.Impulse);
            animator.SetInteger("Estado",1); 
        }
        if (Input.GetKey(KeyCode.X))
        {
            animator.SetInteger("Estado",2); 
        }
        if (pasados >= 10)
        {
            animator.SetInteger("Estado", 3);
            
            rb.velocity = new Vector2(0, rb.velocity.y);
        }
        
    }
     private void OnCollisionEnter2D(Collision2D collision)
    {
        
        
        if (collision.gameObject.CompareTag(tag_enemigo) && Input.GetKey(KeyCode.X))
        {
            
            Destroy(collision.gameObject);
            pasados = pasados + 1;
            velocidad = velocidad + 1.5f;
        }else
        {
             if (collision.gameObject.CompareTag(tag_enemigo))
            {
                Destroy(this.gameObject);
            }
        
        }
       

    }
     private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.CompareTag(tag_cabeza))
        {
            pasados = pasados + 1;
            velocidad = velocidad + 1.5f;
        }
    }
}
